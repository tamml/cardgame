package edu.ntnu.idatt2003;

/**
 * The main class of the application.
 */
public class App {
  public static void main(String[] args) {
    CardGameGUI.launch(CardGameGUI.class, args);
  }
}