package edu.ntnu.idatt2003;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * This class represents the GUI for the card game.
 * And methods to manipulate and analyze them.
 */
public class CardGameGUI extends Application {
  private final DeckOfCards deck = new DeckOfCards();
  private Hand hand;

  /**
   * The main method of the application.
   * It contains buttons and labels for the GUI
   * and methods to manipulate and analyze them.
   *
   * @param stage the primary stage for the application.
   */
  @Override
  public void start(Stage stage) {
    Button dealButton = new Button("Deal Hand");
    Button totalValueButton = new Button("Total Value");
    Button containHeartsButton = new Button("Contain Hearts");
    Button queenOfSpadesButton = new Button("Queen of Spades");
    Button fiveFlushButton = new Button("Five Flush");
    Label dealLabel = new Label();
    Label totalValueLabel = new Label();
    Label containHeartsLabel = new Label();
    Label queenOfSpadesLabel = new Label();
    Label fiveFlushLabel = new Label();

    dealButton.setOnAction(e -> {
      hand = deck.dealHand();
      StringBuilder handStringBuilder = new StringBuilder();
      for (PlayingCard card : hand.getCards()) {
        handStringBuilder.append(card.getAsString()).append(" ");
      }
      dealLabel.setText(handStringBuilder.toString().trim());

      totalValueLabel.setText("");
      containHeartsLabel.setText("");
      queenOfSpadesLabel.setText("");
      fiveFlushLabel.setText("");
    });

    totalValueButton.setOnAction(e -> {
      if (hand != null) {
        int totalValue = deck.getTotalValue(hand);
        totalValueLabel.setText("Total Value: " + totalValue);
      }
    });

    containHeartsButton.setOnAction(e -> {
      if (hand != null) {
        String hearts = deck.containHearts(hand);
        containHeartsLabel.setText("Hearts: " + hearts);
      }
    });

    queenOfSpadesButton.setOnAction(e -> {
      if (hand != null) {
        boolean hasQueenOfSpades = deck.containsQueenOfSpades(hand);
        queenOfSpadesLabel.setText("Contains Queen of Spades: " + hasQueenOfSpades);
      }
    });

    fiveFlushButton.setOnAction(e -> {
      if (hand != null) {
        boolean isFiveFlush = deck.isFiveFlush(hand);
        fiveFlushLabel.setText("Is Five Flush: " + isFiveFlush);
      }
    });

    VBox layout = new VBox(20);
    layout.getChildren().addAll(dealLabel, totalValueLabel, containHeartsLabel, queenOfSpadesLabel, fiveFlushLabel, dealButton, totalValueButton, containHeartsButton, queenOfSpadesButton, fiveFlushButton);

    Scene scene = new Scene(layout, 300, 200);
    stage.setScene(scene);
    stage.setTitle("Card Game");
    stage.show();
  }

  /**
   * The main method of the application.
   * It launches the application.
   *
   * @param args the command-line arguments
   */
  public static void main(String[] args) {
    launch(args);
  }
}

