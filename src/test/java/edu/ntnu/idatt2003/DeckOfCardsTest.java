package edu.ntnu.idatt2003;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardsTest {

  @Test
  public void testDealCard() {
    DeckOfCards deck = new DeckOfCards();
    PlayingCard card = deck.dealCard();
    assertNotNull(card, "Card should not be null");
  }

  @Test
  public void testDealHand() {
    DeckOfCards deck = new DeckOfCards();
    Hand hand = deck.dealHand();
    assertEquals(5, hand.getCards().size(), "Hand should contain 5 cards");
  }

  @Test
  public void testGetTotalValue() {
    DeckOfCards deck = new DeckOfCards();
    Hand hand = deck.dealHand();
    int value = deck.getTotalValue(hand);
    assertTrue(value >= 5 && value <= 65, "Total value should be between 5 and 65");
  }

  public void testGetTotalValue2() {
    DeckOfCards deck = new DeckOfCards();
    Hand hand = deck.dealHand();
    int value = deck.getTotalValue(hand);
    assertTrue(value >= 5 && value <= 65, "Total value should be between 5 and 65");
  }

  @Test
  public void testContainHearts() {
    DeckOfCards deck = new DeckOfCards();
    Hand hand = deck.dealHand();
    String hearts = deck.containHearts(hand);
    assertNotNull(hearts, "Output should not be null");
  }

  @Test
  public void testContainsQueenOfSpades() {
    DeckOfCards deck = new DeckOfCards();
    Hand hand = deck.dealHand();
    boolean containsQueenOfSpades = deck.containsQueenOfSpades(hand);
    assertNotNull(containsQueenOfSpades, "Output should not be null");
  }

  @Test
  public void testIsFiveFlush() {
    DeckOfCards deck = new DeckOfCards();
    Hand hand = deck.dealHand();
    boolean isFiveFlush = deck.isFiveFlush(hand);
    assertNotNull(isFiveFlush, "Output should not be null");
  }
}
