package edu.ntnu.idatt2003;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class HandTest {

  @Test
  public void testAddCard() {
    Hand hand = new Hand();
    PlayingCard card = new PlayingCard('H', 1);
    hand.addCard(card);
    assertEquals(1, hand.getCards().size(), "Hand should contain one card after one card is added");
    assertEquals(card, hand.getCards().get(0), "Hand should contain the added card");
  }

  @Test
  public void testGetTotalValue() {
    Hand hand = new Hand();
    hand.addCard(new PlayingCard('H', 1));
    hand.addCard(new PlayingCard('D', 11));
    hand.addCard(new PlayingCard('C', 12));
    hand.addCard(new PlayingCard('S', 13));
    hand.addCard(new PlayingCard('H', 5));
    assertEquals(36, hand.getTotalValue(), "Hand value should be correct");
  }

  @Test
  public void testGetTotalValueWithEmptyHand() {
    Hand hand = new Hand();
    assertEquals(0, hand.getTotalValue(), "Hand value should be zero when hand is empty");
  }
}
