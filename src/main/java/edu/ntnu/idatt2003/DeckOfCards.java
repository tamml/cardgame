package edu.ntnu.idatt2003;

import java.util.Random;

/**
 * A class representing a deck of playing cards. A deck has 52 cards, 13 of each suit.
 * The deck is used to deal 5 cards to a hand, and to check for specific combinations of cards.
 */
public class DeckOfCards {

  private Random rand = new Random();

  /**
   * Deals a random card from the deck.
   *
   * @return a new deck of cards
   */
  public PlayingCard dealCard() {
    char[] suits = {'H', 'D', 'C', 'S'};
    char suit = suits[rand.nextInt(suits.length)];
    int face = rand.nextInt(13) + 1;
    return new PlayingCard(suit, face);
  }

  /**
   * Deals a hand of 5 cards from the deck.
   *
   * @return a new hand of 5 cards
   */
  public Hand dealHand() {
    Hand hand = new Hand();
    for (int i = 0; i < 5; i++) {
      hand.addCard(dealCard());
    }
    return hand;
  }

  /**
   * Returns the total value of a hand.
   * The value of a hand is the sum of the face value of the cards.
   * The face value of the cards are as follows: 2-10 have their face value,
   * while the jack, queen and king
   * each have a value of 10. The ace has a value of 1.
   *
   * @param hand the hand to calculate the total value of
   * @return the total value of the hand
   */
  public int getTotalValue(Hand hand) {
    return hand.getTotalValue();
  }

  /**
   * Check for which cards in a hand that are hearts.
   * And returns a string representation of the cards.
   *
   * @param hand the hand to represent as a string
   * @return a string representation of the cards in a hand
   */
  public String containHearts(Hand hand) {
    StringBuilder heartsStringBuilder = new StringBuilder();
    boolean hasHearts = false;
    for (PlayingCard card : hand.getCards()) {
      if (card.getSuit() == 'H') {
        heartsStringBuilder.append(card.getAsString()).append(" ");
        hasHearts = true;
      }
    }
    if (!hasHearts) {
      return "No Hearts";
    }
    return heartsStringBuilder.toString().trim();
  }

  /**
   * Check if a card in the hand is the queen of spades.
   *
   * @param hand the hand to represent as a string
   * @return a string representation of the cards in a hand
   */
  public boolean containsQueenOfSpades(Hand hand) {
    for (PlayingCard card : hand.getCards()) {
      if (card.getSuit() == 'S' && card.getFace() == 12) {
        return true;
      }
    }
    return false;
  }

  /**
   * Check if a hand is a five flush.
   * A five flush is a hand with 5 cards of the same suit.
   *
   * @param hand the hand to check
   * @return true if the hand is a five flush, false otherwise
   */
  public boolean isFiveFlush(Hand hand) {
    int heartsCount = 0;
    int diamondsCount = 0;
    int clubsCount = 0;
    int spadesCount = 0;

    for (PlayingCard card : hand.getCards()) {
      char suit = card.getSuit();
      switch (suit) {
        case 'H':
          heartsCount++;
          break;
        case 'D':
          diamondsCount++;
          break;
        case 'C':
          clubsCount++;
          break;
        case 'S':
          spadesCount++;
          break;
        default:
          System.out.println("Unexpected suit value: " + suit);
          break;
      }
    }

    return heartsCount == 5 || diamondsCount == 5 || clubsCount == 5 || spadesCount == 5;
  }

}