package edu.ntnu.idatt2003;

import java.util.ArrayList;
import java.util.List;


/**
 * This class represents a hand of cards.
 * A hand contains a collection of playing cards
 * and provides methods to manipulate and analyze them.
 */
public class Hand {
  /**
   * The list of cards in the hand.
   */
  private List<PlayingCard> cards = new ArrayList<>();

  /**
   * Adds a card to the hand.
   *
   * @param card the card to add
   */
  public void addCard(PlayingCard card) {
    cards.add(card);
  }

  /**
   * The list of cards in the hand.
   *
   * @return the list of cards in the hand
   */
  public List<PlayingCard> getCards() {
    return cards;
  }

  /**
   * Returns the total value of the hand.
   * The value of a hand is the sum of the face value of the cards.
   * The face value of the cards are as follows: 1-9 have their face value,
   * while the jack, queen and king
   * each have a value of 10. The ace has a value of 1.
   *
   * @return the total value of the hand
   */
  public int getTotalValue() {
    int totalValue = 0;
    for (PlayingCard card : cards) {
      int faceValue = card.getFace();
      if (faceValue >= 11 && faceValue <= 13) {
        totalValue += 10;
      } else {
        totalValue += faceValue;
      }
    }
    return totalValue;
  }

}
